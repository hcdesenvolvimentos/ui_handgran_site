
$(function(){
	$( "header .hand-menu-home> ul.menu-nav li" )
	  .mouseover(function(e) {
	   $("div.pg.hand-home>section article").addClass("ativarSombra");
	   $(".areaDestaqueSombra").addClass("ativarSombra_destaque");
	  })
	  .mouseout(function(e) {
	   $("div.pg.hand-home>section article").removeClass("ativarSombra");
	   $(".areaDestaqueSombra").removeClass("ativarSombra_destaque");

	  });

	  $( ".hand-contato-float" ).click(function() {
		 $(".hand-contato-float").slideUp();
		  $("header .hand-menu-home> ul.menu-nav li>a").show();
		  $("div.pg.hand-home>section article").removeClass("ativarSombra")
		  setTimeout(function(){
		  $(".areaDestaqueSombra").removeClass("ativarSombra_destaque");
		}, 1000);
		}); 

	  $( "header .hand-button-menu-mobile button" ).click(function() {
		 $(".hand-contato-float").slideDown();
		 $("header .hand-menu-home> ul.menu-nav li>a").hide();
		  $("div.pg.hand-home>section article").addClass("ativarSombra");
		   $(".areaDestaqueSombra").addClass("ativarSombra_destaque");

		});
	/*****************************************
		SCRIPTS CARROSSEL DESTAQUE
	*******************************************/
	$("#carrosselDestaque").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: false,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	$("#carrosselMenuItem").owlCarousel({
		items : 4,

        dots: true,
        loop: false,
        lazyLoad: false,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:false,
	    smartSpeed: 450,


	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1,
               
            },
            460:{
                items:2,
               
            },
            768:{
                items:3,
               
            },
           
            991:{
                items:3,
               
            },
            1024:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});

	//BOTÕES DO CARROSSEL DESTAQUE HOME
	var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
	$('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
	$('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });



});




